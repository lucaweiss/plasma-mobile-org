---
title:      "Plasma Mobile update: part 11-12"
created_at: 2019-12-06 15:25:00 UTC+2
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the blogpost with updates from week 11 and 12.

## Shell

Carson Black committed a change to [rework the applications splash screen](https://invent.kde.org/kde/plasma-phone-components/merge_requests/31). Now splashscreen follows the colorscheme of the application icon, and shows a shadow around the application icon. Aleix Pol committed changes to share the startup code with the Plasma on Wayland session.

## Applications

### Calindori

Calindori [added support for repeating events](https://invent.kde.org/kde/calindori/commit/6e629788321fa7eeef1ead9a32be6c9937e75e8b). On the event editor, events can be marked as repeating. An overlay sheet enables the users to provide the repeating event details. On the events list view, the event cards show information about event repeats. Several changes were made to improve the layout and user interface, now todo cards and event cards layout are more consistent.

![Calindori repeat editor](/img/screenshots/calindori_repeat_edit.png){: .blog-post-image-small}
![Calindori repeat card view](/img/screenshots/calindori_repeat_list.png){: .blog-post-image-small}

### Ktrip

KTrip now allows the user to select the providers from which public transportation data is retrieved. Users can also disable the un-necessary providers, which produces faster and better results. A change was also committed to make the drawer show up as a menu on desktop.

### Keysmith

Bhushan Shah released [v0.1 of keysmith](https://blog.bshah.in/2019/12/18/keysmith-v0-1-release/), and has also [moved it to kdereview](https://marc.info/?l=kde-core-devel&m=157668067425613&w=2) for moving it to extragear.

### Phonebook

Rituka Patwal added support for [adding](https://invent.kde.org/rpatwal/kpeople-sink/commit/261d58c6c6d689127037e9a5e39f1045acdbec3a) and [removing](https://invent.kde.org/rpatwal/kpeople-sink/commit/4c7eaa0193f6657cae30ba87ee2ad3b9a2437135) contacts in the sink backend for kpeople. This allows user to add and remove contacts from online sources such as Nextcloud; previously they were read-only.

### Okular

Aleix Pol worked on [improving Okular Mobile so it makes use of Kirigami components instead of custom ones](https://invent.kde.org/kde/okular/merge_requests/79) where applicable, which improves look and feel of thumbnails view and table of contents.

![Okular Thumbnails](/img/screenshots/okular_thumbs.png){: .blog-post-image-small}
![Okular Table of contents](/img/screenshots/okular_toc.png){: .blog-post-image-small}

## KF6 sprint

KDE Frameworks team met in the Berlin at MBition offices to discuss the KDE Frameworks 6, They discussed plans for better mobile support in some frameworks, i.e. better separation of core, Widgets UI and QML UI.

## Downstream

postmarketOS now uses the upstream launch scripts rather than custom ones, making it easier to update plasma-phone-components in the future. The [Alpine 3.11 release from this week](https://alpinelinux.org/posts/Alpine-3.11.0-released.html) is the first Alpine release to include KDE Plasma. Included are also some mobile applications like Calindori, Angelfish and Kaidan. postmarketOS is based on Alpine Linux and will have the first stable release based on Alpine 3.11

KDE Neon developers have released [initial images for the PinePhone](https://images.plasma-mobile.org/pinephone/).

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
