---
title:      "Plasma Mobile: weekly update: part 7-8"
created_at: 2019-11-22 17:30:00 UTC+2
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the seventh weekly blogpost. This week's update is full of application updates and phone calls are a work in progress on Pinephone.

_This weekly update contains updates from week 7 and week 8 as we were unable to write an update last week_

## Applications

### Calindori

Following the KDE HIGs, when selecting a date and opening the context (right side) drawer, the selected date is now set as the title of the drawer. That way we make clear to the users the exact date they are working on.

![Calindori Context Drawer](/img/w7_calindori_context_title.png){: .blog-post-image-small}

Some convergence bits have been also added; when Calindori is used on desktop, the global (left side) drawer is transformed to an hierarchy of menu items under a hamburger menu.

![Calindori Convergence](/img/calindori_convergence.png){: .blog-post-image-small}

Under the hood, CMake code has been cleaned up, removing unneeded dependencies and redundant code.

### KTrip

KTrip now shows the additional connection notes.

### Angelfish

Angelfish is now only displayed as "Angelfish" instead of "Angelfish Web Browser" on the homescreen.

## Upstream

Jonah Brüchert [upstreamed more patches to ofono-phonesim](https://lists.ofono.org/hyperkitty/list/ofono@ofono.org/thread/UAKVIJR3OVLWLTEOMNEM5QD7NBOO3IP5/).

## Downstream

Bhushan Shah submitted multiple patches in postmarketOS to integrate telephony functions with user interface. Using which PINE64 Pinephone can connect calls from user interface. Currently audio is a work in progress however, and we hope to have this resolved soon.

<iframe id='ivplayer' type='text/html' width='640' height='360' src='https://invidio.us/embed/83KKWqlzD1s' frameborder='0'></iframe>

## Special Thanks

We would like to thank Mathis Brüchert, for [updating the website design](https://invent.kde.org/websites/plasma-mobile-org/merge_requests/27) to the design very similar to what we introduced in [week 2 update](https://www.plasma-mobile.org/2019/10/11/Plasma-Mobile-Weekly-update-2.html).

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
