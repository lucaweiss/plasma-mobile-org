---
title:      "Plasma Mobile: weekly update: part 1"
created_at: 2019-10-04 10:30:00 UTC+5.30
author:     Plasma Mobile team
layout:     post
---

Starting from today, the Plasma Mobile team is beginning a weekly blog series to highlight the fixes and features landing in various modules that make Plasma Mobile.

## Phone shell

At Akademy Bhushan and Marco [presented Plasma Nano shell](https://conf.kde.org/en/akademy2019/public/events/109) to the community. Earlier this week the changes to use [plasma-nano](https://invent.kde.org/kde/plasma-nano) as a base shell package landed in plasma-phone-components. The shell includes an updated look for the app launcher and several of the shell interactions, including adding and removing widgets and changing the wallpaper.

![Plasma Nano](/img/plasma-nano-week-1.jpeg)

## Kirigami

A very common pattern for applications, both for mobile and desktop, is to include some kind of menu which loads different "main pages" for the application. On a desktop application, you'll have a sidebar on the left which switches the pages on the right. On a mobile application you'll have this list either as the first page or in the left side drawer accessible by swiping right. Since it's a pattern that ended up being needed by many apps, we introduced a new dedicated API for it: PagePool and PagePoolAction: This API makes it possible to implement this paradigm with just few lines of code.

Here is a minimal example of an application that implements this behavior with PagePool and PagePoolAction:

```
import QtQuick 2.6
import org.kde.kirigami 2.11 as Kirigami

Kirigami.ApplicationWindow {
    id: root

    Kirigami.PagePool {
        id: mainPagePool
    }

    globalDrawer: Kirigami.GlobalDrawer {
        title: "Hello App"
        titleIcon: "applications-graphics"

        actions: [
            Kirigami.PagePoolAction {
                text: i18n("Page1")
                icon.name: "speedometer"
                pagePool: mainPagePool
                page: "Page1.qml"
            },
            Kirigami.PagePoolAction {
                text: i18n("Page2")
                icon.name: "window-duplicate"
                pagePool: mainPagePool
                page: "Page2.qml"
            }
        ]
    }
    contextDrawer: Kirigami.ContextDrawer {
        id: contextDrawer
    }

    pageStack.initialPage: mainPagePool.loadPage("Page1.qml")
}
```

See it in action in the following video:

<video controls>
    <source src="/video/kirigami-pagepool.mp4" type="video/mp4"/>
</video>

## Maui Project

[MauiKit, the UI framework](https://mauikit.org/), is now making further usage of Kirigami properties, components and helpers for visual consistency. It has now become more integrated into the platform by using KF5 libraries and has gained new features to improve the user interaction patterns both on mobile and desktop.

![Information module](/img/screenshots/about-info-kcm.png){: .blog-post-image-right}

## Settings application

Nicolas Fella updated the "Settings" app to fix the module activation when the app is already running, [commit](https://invent.kde.org/kde/plasma-settings/merge_requests/33).

Code of the ["Accounts" module was moved](https://commits.kde.org/kaccounts-integration/feb0b036da1ca5a90309f20caa8ee3329da921ed) from the Settings app to `kaccounts-integration`, replacing the existing module there. This makes the desktop and mobile platforms use the same unified code base. 

Jonah Brüchert [added an "Information" module](https://invent.kde.org/kde/plasma-settings/merge_requests/32) in the Settings application, which will eventually replace the the "about-distro" module in the kinfocenter code base.

## Applications

Dan Leinir Turthra Jensen fixed [Peruse](https://cgit.kde.org/peruse.git/about/) making it usable on HighDPI screens. [commit](https://commits.kde.org/peruse/5aaec35aae3ae67b702f7710648537f59a9ae128).

Jonah Brüchert introduced changes in [Plasma Angelfish](https://cgit.kde.org/plasma-angelfish.git/about/) to [port the settings screen](https://commits.kde.org/plasma-angelfish/08ba58f629d5f136c2141ee56ac5990c55c9aedf) to match the Kirigami look-and-feel and navigation and usage patterns. Changes were also introduced to [split the global drawer and context drawer](https://commits.kde.org/plasma-angelfish/98a0d8fb9aee46696f8dbca80d002194145961b9).

Bhushan Shah [fixed a crash in the dialer code](https://invent.kde.org/kde/plasma-phone-components/merge_requests/22) at startup, which was then tested by Luca Weiss on the Pinephone developer kit.

[Index, the file manager](https://invent.kde.org/kde/index-fm), now makes use of KIO for file operations. It also uses the same model for bookmarks and places as the desktop. This means tighter integration with other apps and the system, providing progress notifications on moving, copying and removing files and browsing remotes locations like SFTP.

There is now a collapsible sidebar that, when collapsed, can be dragged to preview its contents. This is useful on small screens, such as on Plasma Mobile devices.

You can now also browse your files with the new Miller Column View, and open different places in different tabs.

With the Selection Bar interaction pattern you can select files across different places. This interaction pattern has been improved a lot and the selection state in the different views and directories is preserved.

Index incorporates a file preview which allows to quickly preview files and get basic information from text, images, videos and audio files. Coming soon: PDFs.

[Nota, the simple text editor](https://invent.kde.org/kde/nota), has gained syntax highlighting support and you can also open multiple files in different tabs thanks to the KIO libraries and KQuickSyntaxHightlighter.

[Buho, the note-taking and link-collector](https://invent.kde.org/kde/buho), can now sync notes by using NextCloud’s Notes app API, and can benefit from MauiKit Editor component for syntax highlighting to save snippets of code.

Some of the Maui apps are about to have stable relases and you can try them out on Android as well!

![Index file manager favorites](/img/screenshots/index-favorites.png){: .blog-post-image}

![Index selection view](/img/screenshots/index-selection.png){: .blog-post-image}

![Index previewing music file](/img/screenshots/index-music-preview.png){: .blog-post-image}

![Nota file editor](/img/screenshots/nota-file-editor.png){: .blog-post-image}

![Buho showing notes](/img/screenshots/buho-notes.png){: .blog-post-image}

![vvave music list](/img/screenshots/vvave-music-list.png){: .blog-post-image}

Johan Ouwerkerk  has made major improvements to the [otpclient](https://invent.kde.org/bshah/otpclient) app in the last few weeks from. otpclient is an app for generating two factor login codes, similar to Google’s Authenticator or SailOTP. 

Currently the basic feature works, but a lot of work remains to be done. One way you can help us a lot is by suggesting a better name for the app on the theme of "keys", "two factor", "login" and "authentication".

## Downstream

In postmarketOS, changes by Clayton Craft to [update the device support for Librem 5 devkit](https://gitlab.com/postmarketOS/pmaports/merge_requests/590) was merged along with changes by Bart Ribbers to [update Plasma to the latest pre-release](https://gitlab.com/postmarketOS/pmaports/merge_requests/639) and [update the settings app to the latest revision](https://gitlab.com/postmarketOS/pmaports/merge_requests/521). Bhushan created a merge request to update the [mesa and kernel used for pinephone in postmarketOS](https://gitlab.com/postmarketOS/pmaports/merge_requests/656). You can watch a video by the postmarketOS developer, Martijn Braam. In the video he puts together the final PinePhone prototype, which also includes a sneak preview of Plasma Mobile!

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/VyeD1sfQNoM?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

You can also watch a demo of Plasma Mobile running on Librem 5 devkit using updated postmarketOS packages:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5aUSVl21jhc?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The KDE Neon team upgraded the Qt version from 5.12.3 to 5.13.1. This includes several bugfixes and new features. These upgrades have landed in the new edge image for Halium based devices like the Nexus 5X.

## Want to help?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](/findyourway). We are also always happy to welcome new contributors on our [public channels](/join). See you there!
