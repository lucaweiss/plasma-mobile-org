---
title:      "Plasma Mobile at Akademy 2018"
created_at: 2018-08-23 15:30:00 UTC+1
author:     Jonah Brüchert, Bhushan Shah
layout:     post
---

Akademy is the annual world summit of KDE. It is a free, non-commercial event organized by the KDE Community. This year's Akademy was hosted at the [TU Wien](https://www.tuwien.ac.at/en/) from the 11th to the 17th of August. Like most KDE community members, Plasma Mobile team members also attended the Akademy and took part in the event, gave [various talks](https://conf.kde.org/en/Akademy2018/public/schedule) and also hosted/participated in various [BoF Sessions](https://community.kde.org/Akademy/2018/AllBoF). We would like to thank the organizers of the event for providing us with a great opportunity to meet, discuss and hack!

<img alt="Plasma Mobile in Vienna" src="/img/plasma-mobile-in-vienna.jpg" width="40%">

## Talks

Akademy 2018 featured several talks that were related to Plasma Mobile in one way or another. Here's a brief overview of some of those talks.

### Day #1

**Bhushan Shah** presented the collaborative efforts of the KDE community to use the mainline kernel instead of Halium on more open devices. Slides from the talk are available [on Bhushan Shah's website.](http://blog.bshah.in/slides/akademy2018/).

<img alt="Bhushan Shah" src="/img/bhushan-shah-talk.jpg" width="40%">

### Day #2

**Oliver Smith** (the founder of postmarketOS) gave a talk on the postmarketOS project. Slides from his talk are available [on the postmarketOS project website.](https://postmarketos.org/static/slides/2018-akademy/).

<img alt="Oliver Smith" src="/img/ollieparanoid-akademy.jpg" width="40%">

**Volker Krause** presented KDE Frameworks 5 for [Yocto Project](https://www.yoctoproject.org/), opensource collaboration project to help developers create custom Linux-based systems for the embedded products. Volker demonstrated Plasma Mobile running on Raspberry Pi 3 using the Yocto project. In another talk, Volker presented the cross-platform application that helps users manage their travel reservations - Itinerary, which also targets Plasma Mobile.

<img alt="Volker Krause" src="/img/volker-plamo-rpi.jpg" width="40%">
<img alt="Volker Krause" src="/img/volker-itinerary.jpg" width="40%">

**Camilo Higuita** talked about VVave, a cross-platform project which uses the Kirigami framework and targets the Plasma Mobile platform in addition to other projects.

<img alt="Camilo Higuita" src="/img/akademy-2018-vvave.jpg" width="40%">

## BoF Sessions

After two days of talks, Akademy progressed into a whole week of BoF (Birds of a Feather) sessions. There were so many great BoFs at this year's Akademy, and we tried to attend as many as possible! These sessions are a valuable opportunity for cross-collaboration between KDE teams. We get to exchange ideas, work together on plans for the future, and resolve problems thanks to fresh perspectives from people outside of our own bubble.

### Plasma Mobile BoF (Monday)

<img alt="Kirigami BoF 2" src="/img/akademy18-plamo-bof.jpg" width="40%">

The Plasma Mobile team hosted the Plasma Mobile BoF on Monday. In our BoF session, we discussed various things with other Plasma developers and community members, and came to the following conclusions:

- We should start making proper releases, to make it easier for packagers to bring Plasma Mobile to the users. Currently it is hard to know if the latest unreleased Plasma Mobile packages depend on an unreleased version of Plasma or not.

- Move KCMs which are not specific to the desktop platform out of plasma-desktop to make them available on Plasma Mobile. We still need to test which modules work fine on a mobile form factor, but we already know we can share, for example, the locale module.

- Bhushan Shah concluded that if we package the Maui Project's apps and write some basic apps that are still missing (like a timer application), Plasma Mobile could actually become usable as a daily driver!

### Flatpak and Snap BoF

We joined the Flatpak and Snap BoF to discuss the state of bundles on Plasma Mobile. Ideally, bundles should Just Work™ on open devices using the mainline kernel and open source drivers. However, currently they don't work with hardware accelerated graphics on libhybris based systems, since they don't include libhybris and don't have the permission to talk to the Android services. We discussed the potential solution for this problem with other developers.

### Onboarding new developers for Plasma Mobile

Plasma Mobile team met with the Neofytos Kolokotronis and Dimitris Kardarakos, who are working on the KDE community goal to improve and streamline the onboarding process for new contributors. We discussed how to establish a strategy to gain new contributors.

- We realized we need to communicate more about the work we do. Some people got the impression that Plasma Mobile wasn't progressing much, so we want to prove the opposite! This post is the first part of putting our plans into action.

- We want to create a list of all apps that are compatible with Plasma Mobile, to make it easier to find out which apps need to be packaged. This will also help new contributors discover applications that they can start working on.

- Another one of our goals is to make it easier to develop apps for Plasma Mobile in an emulator.

- After the BoF session, we were contacted by Louise Stolborg who provided some useful feedback for our [Find Your Way website](https://www.plasma-mobile.org/findyourway). She pointed out some good points about the website, and highlighted the pain points. Based on her feedback, we will work on improving the website in the upcoming days. 

### Kirigami BoF

The Kirigami team hosted their own BoF session to discuss everything Kirigami. You can find more details about the session in the [blog post by Marco Martin.](http://notmart.org/blog/2018/08/akademy-2018/)

<img alt="Kirigami BoF 1" src="/img/akademy18-kirigami-1.jpg" width="40%">
<img alt="Kirigami BoF 2" src="/img/akademy18-kirigami-2.png" width="40%">

### MauiKit and VVave BoF

There were also VVave BoF and MauiKit BoF sessions. More details about them can be found in the [blog post by the developer of MauiKit.](https://medium.com/@temisclopeolimac/maui-roadmap-c303cacf21eb)

## Community

<img alt="The postmarketOS/Plasma Mobile Team" src="/img/community-group-photo.jpg" width="40%">
<img alt="The UBports/Plasma Mobile Team" src="/img/plamo-ubports-flohack.jpg" width="40%">

Of course, Akademy means socialization, and meeting new people! We used this opportunity to meet up with the postmarketOS developer team, UBports developer Florian Leeber, Purism team, one of the Bronze sponsors for KDE Akademy, and many other developers.

## Sounds exciting?

If you want to help us on our journey to implement this year's KDE goals, improve usability and privacy on Plasma Mobile, and come up with an easy way to contribute to software in the mobile space, please join our [communication channels](https://www.plasma-mobile.org/join/), [find a task](https://www.plasma-mobile.org/findyourway), and become one of us!
